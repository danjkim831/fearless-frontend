window.addEventListener('DOMContentLoaded', async () => {
    const statesUrl = 'http://localhost:8000/api/states/';

    try {
        // Make sure to use await to get a response, not the Promise
        const response = await fetch(statesUrl);
        if (response.ok) {
            // We use the json() method to parse the JSON
            // into a Promise of the data, then use 'await' to return
            // JS object data, not the Promise
            const data = await response.json();
            const selectTag = document.getElementById('state');
            selectTag.setAttribute('disabled', true);

            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }
            selectTag.removeAttribute('disabled');

        } else {
            console.log(`Response status: ${response.status}`);
        }

    } catch (e) {
        console.error(e);
    }


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
            // The reset() method resets the form to its original state
            // In this case, it just clears it. That way we know if something happened
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});