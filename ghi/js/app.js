function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card shadow-lg bg-body-tertiary rounded text-start">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${starts} - ${ends}</div>
        </div>
    `;
}

function getTheFullDate(date) {
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();

    return `${month}/${day}/${year}`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log(`Response Status: ${response.status}`);
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();

                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts);
                    const ends = new Date(details.conference.ends);
                    const startDate = getTheFullDate(starts);
                    const endDate = getTheFullDate(ends);
                    const location = details.conference.location.name;

                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);

                    const row = document.querySelector('.row');
                    row.innerHTML += `
                        <div class="col mb-4">
                            ${html}
                        </div>
                    `;
                }
            }

        }

    } catch (e) {
        console.error(e);
    }

});




/*

========================================================================
NOTES:

// Steps for fetching data from an API:

// 1. Fetch the conference data from the API (used in the URL)
// 2. The fetch function returns a Promise. We need to use 'await' before fetching the API
// 3. We need to have the 'async' keyword for the function (in this case, before the anonymous function)
// 3. continued... You cannot use await unless the funcion is an async function!


// // This outputs a Response object with info like type, url, status, ok, etc (THESE ARE JUST HERE TO HELP ME UNDERSTAND)
// const response = await fetch(url);
// console.log(response);

// OUTPUT OF ABOVE: Response {type: 'cors', url: 'http://localhost:8000/api/conferences/', redirected: false, status: 200, ok: true, …}


// Response.json() returns a Promise which resolves with the result of parsing the body text as JSON
// Note that despite the method being named json(), the result is NOT JSON but is instead the result
// of taking JSON as input and parsing it to produce a Javascript object

// In this specific case: we use the json() method on our response to return a Promise
// We make sure to use the 'await' keyword because we would originally get a Promise returned...
// We don't want a Promise... we want the VALUE the Promise will turn into after you 'await' it!


// // This outputs the data for the list of Conferences (THESE ARE JUST HERE TO HELP ME UNDERSTAND)
// const data = await response.json();
// console.log(data);

// OUTPUT OF ABOVE: {conferences: Array(2)} with a toggle to see info about each Conference

========================================================================
*/





// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//             // Figure out what to do when the response is bad
//             console.log(`Response Status: ${response.status}`);
//         } else {
//             // data now holds a Javascript object with the data of the list of Conferences (JSON data -> converted to JS object)
//             const data = await response.json();

//             // Declaring a variable firstConference and assigning it the value of the first conference in the list of conferences:
//             // Declaring a variable nameTag and assigning it the value of the HTML tag with the class '.card-title'
//             const firstConference = data.conferences[0];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = firstConference.name;

//             const detailUrl = `http://localhost:8000${firstConference.href}`;

//             // detailResponse now holds a JS object with the data from the first conference
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 console.log(details);

//                 const description = details.conference.description;
//                 // console.log(description);

//                 const detailTag = document.querySelector('.card-text');
//                 detailTag.innerHTML = description;

//                 const imgTag = document.querySelector('.card-img-top');
//                 imgTag.src = details.conference.location.picture_url;
//             }
//         }
//     } catch (e) {
//         // Figure out what to do if an error is raised
//     }
// });