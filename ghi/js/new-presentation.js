

// Console.logging: http://localhost:3000/new-presentation.html


// THIS IS TO POPULATE THE "CHOOSE A CONFERENCE" DROP DOWN:
window.addEventListener('DOMContentLoaded', async () => {
    const conferenceUrl = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(conferenceUrl);
        // console.log(response);

        if (response.ok) {
            const data = await response.json();
            // console.log(data);
            const selectTag = document.getElementById('conference');

            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.id;
                option.innerHTML = conference.name;
                // console.log(option);
                selectTag.appendChild(option);
            }
        } else {
            console.error(`Response status: ${response.status} ${response.statusText}`);
        }


    } catch (e) {
        console.error(`Fetch error: ${e}`);
    }

    // ON HOLD (NEED TO FIGURE OUT CORRECT API ENDPOINT FOR presentationUrl):::
    // SUBMITTING (POST) THE NEW PRESENTATION FORM:
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const conferenceId = formData.get('conference');
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const json = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: { 'Content-Type': 'application/json' },
        };

        const presentationResponse = await fetch(presentationUrl, fetchConfig);
        if (presentationResponse.ok) {
            formTag.reset();
            const newPresentation = await presentationResponse.json();
            console.log(newPresentation);
        }
    });
});


