

// LISTENING ON PORT: http://localhost:3000/new-conference.html

window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';
    const conferenceUrl = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(locationUrl);
        console.log(response);

        if (response.ok) {
            const locationData = await response.json();
            const selectTag = document.getElementById('location');

            for (let location of locationData.locations) {
                const option = document.createElement('option');
                // option.value = location.id;
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
        } else {
            console.log(`Response status: ${response.status}`);
        }

    } catch (e) {
        console.error(e);
    }


    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        // fromEntries method transforms a list of key-value pairs into an object
        const json = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: { 'Content-Type': 'application/json' },
        }

        const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
        if (conferenceResponse.ok) {
            formTag.reset();
            const newConference = await conferenceResponse.json();
            console.log(newConference);
        }
    });
});