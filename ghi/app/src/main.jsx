import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import './index.css';
// import { createBrowserRouter, RouteProvider } from "react-router-dom";
// import AttendeesList from "./AttendeeList";
// import LocationForm from "./LocationForm";
// import ConferenceForm from "./ConferenceForm";
// import PresentationForm from "./PresentationForm";
// import AttendConferenceForm from "./AttendConferenceForm";



// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <App />,
//     children: [
//       {
//         path: "conferences/new",
//         element: <ConferenceForm />,
//       },
//       {
//         path: "attendees/new",
//         element: <AttendConferenceForm />,
//       },
//       {
//         path: "locations/new",
//         element: <LocationForm />,
//       },
//       {
//         path: "attendees",
//         element: <AttendeesList />,
//       },
//       {
//         path: "presentations/new",
//         element: <PresentationForm />,
//       },
//       {
//         path: "*",
//         element: (
//           <main style={{ padding: "1rem" }}>
//             <p>There&apos;s nothing here!</p>
//           </main>
//         ),
//       },
//     ]
//   },
// ]);



// ReactDOM.createRoot(document.getElementById('root')).render(
//   <React.StrictMode>
//     <RouteProvider router={router} />
//   </React.StrictMode>
// );

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');

  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}

loadAttendees();