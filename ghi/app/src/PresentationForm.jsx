import React, { useState, useEffect } from "react";



export default function PresentationForm() {

    // STATES:

    const [conferences, setConferences] = useState([]);

    // Instead of individually handling each state variable (presenter_name, company_name, title, etc)
    // We initialize the state variable formData to an object that holds all the form fields.
    // Each property in this corresponds to an input field in the new Presentation Form:
    const [formData, setFormData] = useState({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: '',
    });

    // =================================================================================

    // SUBMIT HANDLER (POST on form tag):

    const handleSubmit = async (event) => {
        event.preventDefault();

        const presentationUrl = `http://localhost:8000/api/conferences/${formData.conference}/presentations/`;
        // Since our formData object contains all necessary fields and values,
        // we don't need to write our own data object to collect all the fields/values
        // like we did in handleSubmit of ConferenceForm  component
        const fetchData = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: { 'Content-Type': 'application/json' }
        };

        try {
            const presentationResponse = await fetch(presentationUrl, fetchData);
            if (presentationResponse.ok) {
                const newPresentation = await presentationResponse.json();
                console.log(newPresentation);

                // clear all fields after successful Presentation POST
                setFormData({
                    presenter_name: '',
                    presenter_email: '',
                    company_name: '',
                    title: '',
                    synopsis: '',
                    conference: '',
                });
            } else {
                console.error(`Response status: ${presentationResponse.status} ${presentationResponse.statusText}`);
            }

        } catch (e) {
            console.error(`Fetch error: ${e}`)
        }

    }

    // =================================================================================

    // STATE HANDLER:

    // Again, instead of handling each state change individually, the handleFormChange works collectively.
    // This function updates the formData state variable whenever ANY input field in the form is updated/changed
    // event.target.value = gets the current value of the input field that triggered the change event
    // event.target.name = gets the "name" attribute of the input field that triggered the change event.
    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        // The setFormData obviously updates the formData state.
        // Spread operator is used to create a new object that has all the current values in formData
        // and also updates the field that was changed ([inputName]: value)
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    // =================================================================================

    // Fetching Conference data for dropdown menu (list of coferences fetched from the conferences API):
    const fetchData = async () => {
        const conferenceUrl = 'http://localhost:8000/api/conferences';
        const response = await fetch(conferenceUrl);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    // =================================================================================

    // JSX:

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.presenter_name} placeholder="Presenter name" required type="text" name="presenter_name"
                                id="presenter_name" className="form-control" />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.presenter_email} placeholder="Presenter email" required type="email" name="presenter_email"
                                id="presenter_email" className="form-control" />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.company_name} placeholder="Company name" type="text" name="company_name" id="company_name"
                                className="form-control" />
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.title} placeholder="Title" required type="text" name="title" id="title"
                                className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea onChange={handleFormChange} value={formData.synopsis} name="synopsis" id="synopsis" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.conference} required name="conference" id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => {
                                    return (
                                        <option key={conference.id} value={conference.id}>
                                            {conference.name}
                                        </option>
                                    );
                                })};
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    );
}