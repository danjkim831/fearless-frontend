import React, { useEffect, useState } from "react";



export default function LocationForm() {

    const handleSubmit = async (event) => {
        // the preventDefault method is called/invoked to cancel the default action that happens
        // to the event. In form submissions, the default action is to send a request to the server
        // and reload the whole page. Without using preventDefault, submitting the form would trigger
        // the whole page to reload/send the data from the form to the database.
        // Using preventDefault gives us control over what happens with the data
        // (we can do other tasks without reloading the whole page, send to API, validate, etc)
        event.preventDefault();

        // Create an empty JSON object:
        const data = {};
        data.name = name;
        data.room_count = roomCount;
        data.city = city;
        data.state = state;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        // Object that will configure the fetch request
        const fetchConfig = {
            // HTTP method to be used. case-insenstive so you can use "POST" or "post"
            method: "post",
            // Contains the data to be sent with the request.
            // Stringify converts a JS value to a JSON string
            body: JSON.stringify(data),
            // Check ThunderClient's "Headers" section
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            // Sends the POST request to the server.
            // The fetch func returns a promise of the response object
            // and is then awaited
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                const newLocation = await response.json();
                console.log(newLocation);

                // We need to clear the form after a successful save (post)
                // So we need to set the state to empty strings/use those values for the form elements
                setName('');
                setRoomCount('');
                setCity('');
                setState('');
            } else {
                console.log(`Error ${response.status} ${response.statusText}`);
            }
        } catch (e) {
            console.error(`Fetch error: ${e}`);
        }
    }

    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    // Create the handleNameChange method to take what the users inputs
    // into the form and store it in the state's "name" variable above
    const handleNameChange = (event) => {
        // The "event" parameter is the event that occurred
        // The "target" property is the HTML tag that caused the event
        // (in this case: event.target is the user's input in the form for the location's name)
        // "event.target.value" property is the text that the user typed into the form in input "Name"
        const value = event.target.value;
        setName(value);
    }

    // METHOD THAT UPDATES THE COMPONENT STATE WHEN USER INPUTS IN ROOMCOUNT FIELD:
    const [roomCount, setRoomCount] = useState('');
    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }

    // METHOD THAT UPDATES THE COMPONENT STATE WHEN USER INPUTS IN THE CITY FIELD:
    const [city, setCity] = useState('');
    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    // METHOD THAT UPDATES THE COMPONENT STATE WHEN USER CHOOSES A STATE IN THE STATE FIELD:
    // In ReactDevTools you'll notice that when you pick a State from list, you'll see the abbreviation of the state chosen:
    const [state, setState] = useState('');
    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const [states, setStates] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setStates(data.states);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new location</h1>
                        {/* onSubmit is an event handler that triggers when the form is submitted.
                            It is one of the form events that sends the input data to the handleSubmit function to utilize that information */}
                        <form onSubmit={handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                {/* onChange is an event handler that triggers when there is any change in the input field */}
                                <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" className="form-control" value={name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" id="room_count"
                                    className="form-control" value={roomCount} />
                                <label htmlFor="room_count">Room count</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleCityChange} placeholder="City" required type="text" id="city" className="form-control" value={city} />
                                <label htmlFor="city">City</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleStateChange} required name="state" id="state" className="form-select" value={state}>
                                    <option value="">Choose a state</option>
                                    {states.map(state => {
                                        return (
                                            <option key={state.abbreviation} value={state.abbreviation}>
                                                {state.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );

}






/*

We're creating a STATEFUL COMPONENT here.
This means that it can have internal data saved
in the component when it gets created.
(just like our model classes in Django)

All React components need a return method which returns
a single HTML element as JSX. Look at the <div className="container"> tag.
That JSX will get turned into an actual paragraph by React.


----------------------------------------------------------


There's a hook called useEffect() that we can use 
to make fetch requests to get the data we need.



*/
