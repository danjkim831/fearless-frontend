import { NavLink } from "react-router-dom";

export default function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                {/* <a className="navbar-brand" href="#">Conference GO!</a> */}
                <NavLink to="/" className="navbar-brand">Conference GO!</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            {/* <a className="nav-link active" aria-current="page" href="#">Home</a> */}
                            <NavLink to="/" className="nav-link active" aria-current="page">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            {/* <a className="nav-link" href="#">New location</a> */}
                            <NavLink to="/locations/new/" className="nav-link">New location</NavLink>
                        </li>
                        <li className="nav-item">
                            {/* <a className="nav-link" href="#">New conference</a> */}
                            <NavLink to="/conferences/new/" className="nav-link">New conference</NavLink>
                        </li>
                        <li className="nav-item">
                            {/* <a className="nav-link" href="#">New presentation</a> */}
                            <NavLink to="presentations/new/" className="nav-link">New presentation</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav >
    );
}

// Either have this here or right before the component signature:
// export default Nav;
