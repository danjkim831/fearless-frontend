import React, { useState, useEffect } from "react";



export default function ConferenceForm() {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);


        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        }

        try {
            const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
            if (conferenceResponse.ok) {
                const newConference = await conferenceResponse.json();
                console.log(newConference);

                setName('');
                setStartDate('');
                setEndDate('');
                setDescription('');
                setMaxPresentations('');
                setMaxAttendees('');
                setLocation('');
            } else {
                console.error(`Response status: ${conferenceResponse.status} ${conferenceResponse.statusText}`);
            }
        } catch (e) {
            console.error(`Fetch error: ${e}`);
        }
    }

    // =================================================================================

    // SINGLE STATE OBJECTS VARIABLES/HANDLERS:

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [startDate, setStartDate] = useState('');
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const [endDate, setEndDate] = useState('');
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    // =================================================================================

    // FETCHING ALL LOCATIONS FOR DROPDOWN MENU:

    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            // console.log(data);
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    // =================================================================================

    // JSX:

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                                className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartDateChange} placeholder="Starts" required type="date" name="starts" id="starts"
                                className="form-control" value={startDate} />
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndDateChange} placeholder="Ends" required type="date" name="ends" id="ends"
                                className="form-control" value={endDate} />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea onChange={handleDescriptionChange} name="description" id="description" className="form-control" value={description}></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number"
                                name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations} />
                            <label htmlFor="max_presentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees"
                                id="max_attendees" className="form-control" value={maxAttendees} />
                            <label htmlFor="max_attendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location}>
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    );
}