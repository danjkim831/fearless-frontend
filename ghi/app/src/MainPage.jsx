import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ConferenceColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const conference = data.conference;
                return (
                    <div key={conference.href} className="card mb-3 shadow">
                        <img src={conference.location.picture_url} className="card-img-top" />
                        <div className="card-body">
                            <h5 className="card-title">{conference.name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                {conference.location.name}
                            </h6>
                            <p className="card-text">
                                {conference.description}
                            </p>
                        </div>
                        <div className="card-footer">
                            {new Date(conference.starts).toLocaleDateString()}
                            -
                            {new Date(conference.ends).toLocaleDateString()}
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

const MainPage = (props) => {
    const [conferenceColumns, setConferenceColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                // Get the list of conferences
                const data = await response.json();
                console.log(data);  // DK: Returns an array of all conference objects

                // Create a list of for all the requests and
                // add all of the requests to it
                const requests = [];
                for (let conference of data.conferences) {
                    const detailUrl = `http://localhost:8000${conference.href}`;
                    requests.push(fetch(detailUrl));
                }

                // DK: Check formed requests list
                // This returns an array of Promises so you'll need to await all of them
                console.log(requests);

                // Wait for all of the requests to finish
                // simultaneously
                const responses = await Promise.all(requests);
                // DK: Check all responses in the array after await on every single Promise
                // One of the responses in the array:
                // Response {type: 'cors', url: 'http://localhost:8000/api/conferences/1/', redirected: false, status: 200, ok: true, ...}
                console.log(responses);

                // Set up the "columns" to put the conference
                // information into
                const columns = [[], [], []];

                // Loop over the conference detail responses and add
                // each to the proper "column" if the response is ok
                let i = 0;
                for (const conferenceResponse of responses) {
                    if (conferenceResponse.ok) {
                        const details = await conferenceResponse.json();
                        // DK: Details of current conference: {conference: {...}, weather: {...}}
                        // Push current conference details into respective column above
                        // console.log(details);
                        columns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(conferenceResponse);
                    }
                }

                // DK: In this specific project, we get an array of 3 arrays nested inside of it
                // Within the outer array,
                // The first nested array has 2 Conference's (details) in it (2 objects)
                // The second nested array has 2 Conference's (details) in it (2 objects)
                // The third nested array has 1 Conference (details) in it (1 object)
                console.log(columns);

                // Set the state to the new list of three lists of
                // conferences
                setConferenceColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    // // DK: Check your state variable ConferenceColumns after everything is populated
    // // In this specific portion of project, it returns: [ Array(2objectsinside), Array(2objectsinside), Array(1objectinside) ]
    // console.log(conferenceColumns);

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">Conference GO!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                        The only resource you'll ever need to plan an run your in-person or
                        virtual conference for thousands of attendees and presenters.
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="/attendees/new" className="btn btn-primary btn-lg px-4 gap-3">Attend a conference</Link>
                    </div>
                </div>
            </div>
            <div className="container">
                <h2>Upcoming conferences</h2>
                <div className="row">
                    {conferenceColumns.map((conferenceList, index) => {
                        return (
                            <ConferenceColumn key={index} list={conferenceList} />
                        );
                    })}
                </div>
            </div>
        </>
    );
}

export default MainPage;