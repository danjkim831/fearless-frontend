import Nav from "./Nav";
import AttendeesList from "./AttendeeList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import PresentationForm from "./PresentationForm";
import AttendConferenceForm from "./AttendConferenceForm";
import MainPage from "./MainPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) return null;

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="attendees/new/" element={<AttendConferenceForm />} />
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div >
    </BrowserRouter>
  );
}

export default App;

// function App(props) {
//   if (props.attendees === undefined) return null;

//   return (
//     <>
//       <Nav />
//       <div className="container">
//         <AttendeesList attendees={props.attendees} />
//       </div >
//     </>
//   );
// }

// export default App;



// =====================================================

// Both are valid (haven't decided which I prefer yet):
// // Destructured props argument to be more clear above:
// // Below is original way (you have to preface with props attribute):

// function App(props) {
//   if (props.attendees === undefined) return null;

//   return (
//     <>
//       <Nav />
//       <div className="container">
//         <AttendeesList attendees={props.attendees} />
//       </div >
//     </>
//   );
// }

// export default App;

// =====================================================